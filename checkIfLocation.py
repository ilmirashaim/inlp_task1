from __future__ import division
import sys
import xml.etree.ElementTree as ET
import urllib
import re
import numpy as np


def namespace(element):
    m = re.match('\{(.*)\}',element.tag)
    return m.group(1) if m else ''

def isLocation(str):
    params = urllib.urlencode({'QueryString': str})
    f = urllib.urlopen('http://lookup.dbpedia.org/api/search/KeywordSearch?%s' % params)
    data = f.read()

    root = ET.fromstring(data)
    ns = namespace(root)

    result = root.find("{%s}Result" % ns)

    if result is None:
        return False

    def hasClassPlace(el):
        classes = el.findall("{%s}Classes/{%s}Class" % (ns, ns))
        classes = filter(lambda x: x.findtext("{%s}Label" % ns).lower() == 'place', classes)
        return len(classes) > 0

    return hasClassPlace(result)


def main(argv):
#    if (len(argv) == 0):
#        print 'input is empty!'
#        return
#    str = argv[0]
    accuracies = []
    for fName in ['City', 'Region', 'Location', 'Country']:
	f = open('locs_'+fName+'.txt', 'r')
	res = [] 
	for line in f:
	    res.append(isLocation(line))
	ac = (np.sum(res)/len(res))
	print(ac)
	accuracies.append(ac)
    print(sum(accuracies)/len(accuracies))


if __name__ == "__main__":
    main(sys.argv[1:])


